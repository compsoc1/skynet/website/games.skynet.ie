{
  description = "Skynet Games Site";

  inputs = {
    utils.url = "github:numtide/flake-utils";
  };

  outputs = {
    self,
    nixpkgs,
    utils,
  }:
    utils.lib.eachDefaultSystem (system: let
      pkgs = nixpkgs.legacyPackages."${system}";
    in rec {
      # `nix build`
      defaultPackage = pkgs.stdenv.mkDerivation {
        name = "games_skynet_ie";
        src = self;
        installPhase = "mkdir -p $out; cp -R src/* $out";
      };
    });
}
